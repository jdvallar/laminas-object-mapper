<?php


namespace Vallarj\Laminas\ObjectMapper;


class Module
{
    public function getConfig(): array
    {
        return (new ConfigProvider())->getModuleConfiguration();
    }
}