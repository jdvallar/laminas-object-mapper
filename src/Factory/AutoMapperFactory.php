<?php


namespace Vallarj\Laminas\ObjectMapper\Factory;


use Vallarj\ObjectMapper\AutoMapper\AutoMapper;
use Vallarj\ObjectMapper\AutoMapper\Configuration;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AutoMapperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $libConfig = $config['php-object-mapper'];

        $autoMapperConfig = new Configuration($libConfig['configuration']);

        $autoMapper = new AutoMapper($autoMapperConfig);

        $mappers = $libConfig['mappers'];
        foreach($mappers as $mapper) {
            $autoMapper->register($container->get($mapper));
        }

        return $autoMapper;
    }
}