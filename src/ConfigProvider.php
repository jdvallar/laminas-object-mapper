<?php


namespace Vallarj\Laminas\ObjectMapper;


use Vallarj\ObjectMapper\AutoMapper;

class ConfigProvider
{
    /**
     * Retrieve configuration for laminas-object-mapper.
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'php-object-mapper' => $this->getObjectMapperConfiguration(),
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the module configuration for laminas-mvc projects
     *
     * @return array
     */
    public function getModuleConfiguration(): array
    {
        return [
            'php-object-mapper' => $this->getObjectMapperConfiguration(),
            'service_manager' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the default object mapper configuration
     *
     * @return array
     */
    public function getObjectMapperConfiguration(): array
    {
        return [
            'configuration' => [
                AutoMapper\Configuration::MAX_DEPTH => 3
            ],
            'mappers' => []
        ];
    }

    /**
     * Returns the container dependencies.
     * Maps factory interfaces to factories.
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                AutoMapper\AutoMapper::class => Factory\AutoMapperFactory::class
            ],
        ];
    }
}
